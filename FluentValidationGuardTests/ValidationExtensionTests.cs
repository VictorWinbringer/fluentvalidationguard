﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using FluentValidationGuard;
using System;
using System.Collections.Generic;
using System.Text;

namespace FluentValidationGuard.Tests
{
    [TestClass()]
    public class ValidationExtensionTests
    {
        [TestMethod()]
        public void NotNullTest()
        {
            Assert.Fail();
        }

        [TestMethod()]
        public void NotDefaultTest()
        {
            Assert.Fail();
        }

        [TestMethod()]
        public void NotWhiteSpaceTest()
        {
            Assert.Fail();
        }

        [TestMethod()]
        public void LengthNotEqualTest()
        {
            Assert.Fail();
        }

        [TestMethod()]
        public void LengthEqualTest()
        {
            Assert.Fail();
        }

        [TestMethod()]
        public void LengthGreaterTest()
        {
            Assert.Fail();
        }

        [TestMethod()]
        public void LengthLessTest()
        {
            Assert.Fail();
        }

        [TestMethod()]
        public void IsGreaterTest()
        {
            Assert.Fail();
        }

        [TestMethod()]
        public void IsLowerTest()
        {
            Assert.Fail();
        }

        [TestMethod()]
        public void IsGreaterOrEqualsTest()
        {
            Assert.Fail();
        }

        [TestMethod()]
        public void IsLowerOrEqualsTest()
        {
            Assert.Fail();
        }

        [TestMethod()]
        public void IsEqualsTest()
        {
            Assert.Fail();
        }

        [TestMethod()]
        public void NotEqualsTest()
        {
            Assert.Fail();
        }

        [TestMethod()]
        public void MatchTest()
        {
            Assert.Fail();
        }
    }
}