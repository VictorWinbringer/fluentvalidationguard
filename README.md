## IValidate you input!

        ```c#
        public static void Example(string name, int age)
        {
            try
            {
                Validator
                    .Begin(name, nameof(name))
                    .NotNull()
                    .NotWhiteSpace()
                    .Map(age, nameof(age))
                    .IsGreaterOrEquals(18)
                    .TrowIfHasErrors(nameof(ErrorConverter), nameof(Example));
            }
            catch (ValidationException e)
            {
                Console.WriteLine(e);
                IErrorConverter converter = new ErrorConverter();
                var errors = converter.Convert(e);
                Console.WriteLine(errors);
            }
        }
        ```