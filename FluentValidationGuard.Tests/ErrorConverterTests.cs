﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Text.Json;
using FluentAssertions;
using Xunit;
using Xunit.Abstractions;

namespace FluentValidationGuard.Tests
{
    public class ErrorConverterTests
    {
        private readonly ITestOutputHelper _output;

        public ErrorConverterTests(ITestOutputHelper output)
        {
            _output = output;
        }

        [Fact]
        public void Should_ReturnExpectedValue()
        {
            var nx = new ArgumentNullException("1", "11");
            var ox = new ArgumentOutOfRangeException("2", "22");
            var ax = new AggregateException(nx, ox);
            var converter = new ErrorConverter();

            var result = converter.Convert(ax);

            result
                .Should()
                .NotBeNull()
                .And
                .HaveCount(2)
                .And
                .Contain(x => Equals(x, ox))
                .And
                .Contain(x => Equals(x, nx));
            _output.WriteLine(JsonSerializer.Serialize(result));
        }

        private bool Equals(Error lhs, Exception rhs)
        {
            if (lhs.Type != rhs.GetType().FullName)
                return false;
            if (lhs.Message != rhs.Message)
                return false;
            if (lhs.Data != rhs.Data)
                return false;
            return true;
        }
    }
}
