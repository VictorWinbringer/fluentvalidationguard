﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using FluentAssertions;
using Xunit;
using Xunit.Abstractions;

namespace FluentValidationGuard.Tests
{
    public class ValidationExtensionsTest
    {
        private readonly ITestOutputHelper _output;

        public ValidationExtensionsTest(ITestOutputHelper output)
        {
            _output = output;
        }

        private void Print(IEnumerable<ValidationError> errors)
        {
            _output.WriteLine($"count: {errors.Count()}");
            foreach (var validationError in errors)
            {
                _output.WriteLine($"argument: {validationError.ArgumentName}; code:{validationError.Code}");
            }
        }

        private void NotHaveErrors<T>(ValidationResult<T> result)
        {
            Print(result.Errors);
            result.Errors.Should().HaveCount(0);
        }

        private void HaveError<T>(ValidationResult<T> result, ValidationErrorCode code, string argumentName)
        {
            Print(result.Errors);
            result.Errors.Should().HaveCount(1);
            result.Errors.Should().Contain(x =>
                x.ArgumentName == argumentName && x.Code == code.ToString("G"));
        }

        [Fact]
        public void NotNullShould_SkipValidValues()
        {
            string input = "foo";

            var result = Validator
                  .Begin(input, nameof(input))
                  .NotNull();

            NotHaveErrors(result);
        }

        [Fact]
        public void NotNullShould_FindInvalidValues()
        {
            string input = null;

            var result = Validator
                .Begin(input, nameof(input))
                .NotNull();

            HaveError(result, ValidationErrorCode.IsNull, nameof(input));
        }

        [Fact]
        public void NotDefaultShould_SkipValidValues()
        {
            int input = 1;

            var result = Validator
                .Begin(input, nameof(input))
                .NotDefault();
            NotHaveErrors(result);
        }

        [Fact]
        public void NotDefaultShould_FindInvalidValues()
        {
            int input = 0;

            var result = Validator
                .Begin(input, nameof(input))
                .NotDefault();

            HaveError(result, ValidationErrorCode.IsDefault, nameof(input));
        }

        [Fact]
        public void NotDefaultNullableShould_SkipValidValues()
        {
            int? input = 1;

            var result = Validator
                .Begin(input, nameof(input))
                .NotDefault();

            NotHaveErrors(result);
        }

        [Fact]
        public void NotDefaultNullableShould_FindInvalidValues()
        {
            int? input = 0;

            var result = Validator
                .Begin(input, nameof(input))
                .NotDefault();

            HaveError(result, ValidationErrorCode.IsDefault, nameof(input));
        }

        [Fact]
        public void NotWhiteSpaceShould_FindInvalidValues()
        {
            string input = " ";

            var result = Validator
                .Begin(input, nameof(input))
                .NotWhiteSpace();

            HaveError(result, ValidationErrorCode.IsWhiteSpace, nameof(input));
        }

        [Fact]
        public void NotWhiteSpaceShould_SkipValidValues()
        {
            string input = "1";

            var result = Validator
                .Begin(input, nameof(input))
                .NotWhiteSpace();

            NotHaveErrors(result);
        }


        [Fact]
        public void ContainsShould_FindInvalidValues()
        {
            var input = new List<int>() { 1, 2 };

            var result = Validator
                .Begin(input, nameof(input))
                .IsContains((int x) => x == 3);

            HaveError(result, ValidationErrorCode.NotContains, nameof(input));
        }

        [Fact]
        public void ContainsShould_SkipValidValues()
        {
            var input = new List<int>() { 1, 2 };

            var result = Validator
                .Begin(input, nameof(input))
                .IsContains((int x) => x == 2);

            NotHaveErrors(result);
        }


        [Fact]
        public void LengthNotEqualShould_FindInvalidValues()
        {
            var input = "11";

            var result = Validator
                .Begin(input, nameof(input))
                .LengthNotEqual(2);

            HaveError(result, ValidationErrorCode.LengthEqual, nameof(input));
        }

        [Fact]
        public void LengthNotEqualShould_SkipValidValues()
        {
            var input = new List<int>() { 1, 2 };

            var result = Validator
                .Begin(input, nameof(input))
                .LengthNotEqual(3);

            NotHaveErrors(result);
        }

        [Fact]
        public void LengthEqualShould_FindInvalidValues()
        {
            var input = "11";

            var result = Validator
                .Begin(input, nameof(input))
                .LengthIsEqual(3);

            HaveError(result, ValidationErrorCode.LengthNotEqual, nameof(input));
        }

        [Fact]
        public void LengthEqualShould_SkipValidValues()
        {
            var input = new List<int>() { 1, 2 };

            var result = Validator
                .Begin(input, nameof(input))
                .LengthIsEqual(2);

            NotHaveErrors(result);
        }

        [Fact]
        public void LengthGreaterShould_FindInvalidValues()
        {
            var input = "11";

            var result = Validator
                .Begin(input, nameof(input))
                .LengthIsGreater(3);

            HaveError(result, ValidationErrorCode.LengthNotGreater, nameof(input));
        }

        [Fact]
        public void LengthGreaterShould_SkipValidValues()
        {
            var input = new List<int>() { 1, 2 };

            var result = Validator
                .Begin(input, nameof(input))
                .LengthIsGreater(1);

            NotHaveErrors(result);
        }

        [Fact]
        public void LengthLessShould_FindInvalidValues()
        {
            var input = "11";

            var result = Validator
                .Begin(input, nameof(input))
                .LengthIsLess(2);

            HaveError(result, ValidationErrorCode.LengthNotLess, nameof(input));
        }

        [Fact]
        public void LengthLessShould_SkipValidValues()
        {
            var input = new List<int>() { 1, 2 };

            var result = Validator
                .Begin(input, nameof(input))
                .LengthIsLess(3);

            NotHaveErrors(result);
        }

        [Fact]
        public void IsGreaterShould_FindInvalidValues()
        {
            var input = 1;

            var result = Validator
                .Begin(input, nameof(input))
                .IsGreater(1);

            HaveError(result, ValidationErrorCode.NotGreater, nameof(input));
        }

        [Fact]
        public void IsGreaterShould_SkipValidValues()
        {
            var input = 1;

            var result = Validator
                .Begin(input, nameof(input))
                .IsGreater(0);

            NotHaveErrors(result);
        }

        [Fact]
        public void IsLowerShould_FindInvalidValues()
        {
            var input = 1;

            var result = Validator
                .Begin(input, nameof(input))
                .IsLess(1);

            HaveError(result, ValidationErrorCode.NotLess, nameof(input));
        }

        [Fact]
        public void IsLowerShould_SkipValidValues()
        {
            var input = 1;

            var result = Validator
                .Begin(input, nameof(input))
                .IsLess(2);

            NotHaveErrors(result);
        }

        [Fact]
        public void IsGreaterOrEqualsShould_FindInvalidValues()
        {
            var input = 1;

            var result = Validator
                .Begin(input, nameof(input))
                .IsGreaterOrEquals(2);

            HaveError(result, ValidationErrorCode.NotGreaterOrEqual, nameof(input));
        }

        [Fact]
        public void IsGreaterOrEqualsShould_SkipValidValues()
        {
            var input = 1;

            var result = Validator
                .Begin(input, nameof(input))
                .IsGreaterOrEquals(1)
                .IsGreaterOrEquals(0);

            NotHaveErrors(result);
        }

        [Fact]
        public void IsLowerOrEqualsShould_FindInvalidValues()
        {
            var input = 1;

            var result = Validator
                .Begin(input, nameof(input))
                .IsLessOrEquals(0);

            HaveError(result, ValidationErrorCode.NotLessOrEqual, nameof(input));
        }

        [Fact]
        public void IsLowerOrEqualsShould_SkipValidValues()
        {
            var input = 1;

            var result = Validator
                .Begin(input, nameof(input))
                .IsLessOrEquals(1)
                .IsLessOrEquals(2);

            NotHaveErrors(result);
        }

        [Fact]
        public void IsEqualsShould_FindInvalidValues()
        {
            var input = 1;

            var result = Validator
                .Begin(input, nameof(input))
                .IsEquals(2);

            HaveError(result, ValidationErrorCode.NotEqual, nameof(input));
        }

        [Fact]
        public void IsEqualsShould_SkipValidValues()
        {
            var input = 1;

            var result = Validator
                .Begin(input, nameof(input))
                .IsEquals(1);

            NotHaveErrors(result);
        }

        [Fact]
        public void NotEqualsShould_FindInvalidValues()
        {
            var input = 1;

            var result = Validator
                .Begin(input, nameof(input))
                .NotEquals(1);

            HaveError(result, ValidationErrorCode.IsEqual, nameof(input));
        }

        [Fact]
        public void NotEqualsShould_SkipValidValues()
        {
            var input = 1;

            var result = Validator
                .Begin(input, nameof(input))
                .NotEquals(2);

            NotHaveErrors(result);
        }

        [Fact]
        public void IsMatchShould_FindInvalidValues()
        {
            var input = "1A";

            var result = Validator
                .Begin(input, nameof(input))
                .IsMatch("77");

            HaveError(result, ValidationErrorCode.NotMatch, nameof(input));
        }

        [Fact]
        public void IsMatchShould_SkipValidValues()
        {
            var input = "1A";

            var result = Validator
                .Begin(input, nameof(input))
                .IsMatch(@"\d\w");

            NotHaveErrors(result);
        }

        [Fact]
        public void IsMatchShould_SkipValidValuesWithOptions()
        {
            var input = "1A";

            var result = Validator
                .Begin(input, nameof(input))
                .IsMatch(@"\da", RegexOptions.IgnoreCase);

            NotHaveErrors(result);
        }

        [Fact]
        public void NotEmptyShould_FindInvalidValues()
        {
            var input = "";

            var result = Validator
                .Begin(input, nameof(input))
                .NotEmpty();

            HaveError(result, ValidationErrorCode.LengthNotGreater, nameof(input));
        }

        [Fact]
        public void NotEmptyShould_SkipValidValues()
        {
            var input = "1A";

            var result = Validator
                .Begin(input, nameof(input))
                .NotEmpty();

            NotHaveErrors(result);
        }
    }
}
