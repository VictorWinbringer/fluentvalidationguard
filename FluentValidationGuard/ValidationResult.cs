﻿using System;
using System.Collections.Generic;

namespace FluentValidationGuard
{
    public sealed class ValidationResult<T>
    {
        public T Value { get; }
        public string ValueName { get; }
        public IReadOnlyCollection<ValidationError> Errors => _errors.AsReadOnly();

        private readonly List<ValidationError> _errors;
        public ValidationResult(T value, string valueName)
            : this(value, valueName, new List<ValidationError>())
        {

        }
        private ValidationResult(T value, string valueName, List<ValidationError> errors)
        {
            Value = value;
            ValueName = valueName;
            _errors = errors;
        }

        public ValidationResult<T> Validate(Func<T, bool> validationPredicate, Func<string, ValidationError> errorFactory)
        {
            if (validationPredicate(Value))
                return this;
            var ex = errorFactory(ValueName);
            _errors.Add(ex);
            return this;
        }
        public ValidationResult<TResult> Map<TResult>(TResult value, string valueName) => new ValidationResult<TResult>(value, valueName, _errors);

        public ValidationResult<T> TrowIfHasErrors(string objectName, string methodName)
        {
            if (_errors.Count == 0)
                return this;
            throw new ValidationException(objectName, methodName, _errors);
        }

        public ValidationResult<T> TrowIfHasErrors(Func<List<ValidationError>, Exception> factory)
        {
            if (_errors.Count == 0)
                return this;
            throw factory(_errors);
        }
    }
}
