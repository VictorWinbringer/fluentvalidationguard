﻿using System;
using System.Linq;

namespace FluentValidationGuard
{
    public static class Validator
    {
        public static ValidationResult<T> Begin<T>(T value, string valueName) => new ValidationResult<T>(value, valueName);

        public static void Example(string name, int age)
        {
            try
            {
                Validator
                    .Begin(name, nameof(name))
                    .NotNull()
                    .NotWhiteSpace()
                    .Map(age, nameof(age))
                    .IsGreaterOrEquals(18)
                    .TrowIfHasErrors(nameof(ErrorConverter), nameof(Example));
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                IErrorConverter converter = new ErrorConverter();
                var errors = converter.Convert(e);
                Console.WriteLine(errors);
            }
        }
    }
}