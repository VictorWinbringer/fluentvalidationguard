﻿using System;
using System.Collections.Generic;

namespace FluentValidationGuard
{
    public interface IErrorConverter
    {
        List<Error> Convert(Exception ex);
    }
}