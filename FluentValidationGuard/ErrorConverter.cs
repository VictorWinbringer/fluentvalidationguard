﻿using System;
using System.Collections.Generic;

namespace FluentValidationGuard
{
    public sealed class ErrorConverter : IErrorConverter
    {
        private const int MAX_EXCEPTIONS = 10_000;

        public List<Error> Convert(Exception ex)
        {
            var errors = new List<Error>();
            var exceptions = new List<Exception>();
            Add(ex, errors, exceptions);
            return errors;
        }

        private void Add(Exception ex, List<Error> errors, List<Exception> exceptions)
        {
            if (ex == null)
                return;
            if (exceptions.Contains(ex))
                return;
            if (exceptions.Count > MAX_EXCEPTIONS)
                return;
            exceptions.Add(ex);
            if (ex is AggregateException aggregate)
            {
                foreach (var e in aggregate.InnerExceptions)
                {
                    Add(e, errors, exceptions);
                }
            }
            else
            {
                errors.Add(new Error(ex));
            }
            Add(ex.InnerException, errors, exceptions);
        }
    }
}