﻿using System.Collections.Generic;

namespace FluentValidationGuard
{
    public class ValidationError
    {
        public ValidationError(string code, string argumentName, Dictionary<string, object> data)
        {
            Code = code;
            ArgumentName = argumentName;
            Data = data;
        }

        public string Code { get; }
        public string ArgumentName { get; }
        public IReadOnlyDictionary<string, object> Data { get; }
    }
}