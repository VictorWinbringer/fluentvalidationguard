﻿using System;
using System.Collections;
using System.Linq;

namespace FluentValidationGuard
{
    public sealed class Error
    {
        public Error(Exception ex)
        {
            Data = ex.Data;
            Message = ex.Message;
            Type = ex.GetType().FullName;
            if (ex is ValidationException v)
            {
                Data[nameof(v.Errors)] = v.Errors.ToList();
                Data[nameof(v.MethodName)] = v.MethodName;
                Data[nameof(v.ObjectName)] = v.ObjectName;
            }
            if (ex is ArgumentException ae)
                Data[nameof(ae.ParamName)] = ae.ParamName;
        }

        public IDictionary Data { get; }
        public string Message { get; }
        public string Type { get; }
    }
}