﻿namespace FluentValidationGuard
{
    public enum ValidationErrorCode
    {
        IsDefault,
        IsNull,
        IsWhiteSpace,
        NotGreater,
        NotLess,
        NotGreaterOrEqual,
        NotLessOrEqual,
        NotEqual,
        IsEqual,
        NotMatch,
        LengthNotGreater,
        LengthNotLess,
        LengthNotEqual,
        LengthEqual,
        NotContains
    }
}