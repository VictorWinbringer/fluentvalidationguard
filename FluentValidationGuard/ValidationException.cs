﻿using System;
using System.Collections.Generic;

namespace FluentValidationGuard
{
    public class ValidationException : ApplicationException
    {
        public string ObjectName { get; }
        public string MethodName { get; }

        private readonly List<ValidationError> _errors;
        public IReadOnlyCollection<ValidationError> Errors => _errors.AsReadOnly();
        public ValidationException(string objectName, string methodName, List<ValidationError> errors)
        {
            ObjectName = objectName;
            MethodName = methodName;
            _errors = errors;
        }
    }
}