﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;

namespace FluentValidationGuard
{
    public static class ValidationExtension
    {
        public static ValidationResult<T> NotNull<T>(this ValidationResult<T> result)
            => result.Validate(
                x => !(x is null),
                x => new ValidationError(ValidationErrorCode.IsNull.ToString("G"), x, new Dictionary<string, object>())
            );

        public static ValidationResult<T> NotDefault<T>(this ValidationResult<T> result) where T : struct
            => result.Validate(
                x => !x.Equals(default(T)),
                x => new ValidationError(ValidationErrorCode.IsDefault.ToString("G"), x, new Dictionary<string, object>())
            );

        public static ValidationResult<T?> NotDefault<T>(this ValidationResult<T?> result) where T : struct
            => result.Validate(
                x => !x.HasValue || !x.Value.Equals(default(T)),
                x => new ValidationError(ValidationErrorCode.IsDefault.ToString("G"), x, new Dictionary<string, object>())
            );

        public static ValidationResult<string> NotWhiteSpace(this ValidationResult<string> result)
            => result.Validate(
                x => x == null || !string.IsNullOrWhiteSpace(x),
                x => new ValidationError(ValidationErrorCode.IsWhiteSpace.ToString("G"), x, new Dictionary<string, object>())
                );

        public static ValidationResult<U> IsContains<T, U>(this ValidationResult<U> result, Func<T, bool> predicate) where U : IEnumerable<T>
            => result.Validate(
                x => (x is null) || x.Any(predicate),
                x => new ValidationError(ValidationErrorCode.NotContains.ToString("G"), x, new Dictionary<string, object>())
            );

        public static ValidationResult<T> LengthNotEqual<T>(this ValidationResult<T> result, int value) where T : IEnumerable
            => result.Validate(
                x => (x is null) || x.Count() != value,
                x => new ValidationError(ValidationErrorCode.LengthEqual.ToString("G"), x, new Dictionary<string, object>() { ["value"] = value })
                );

        public static ValidationResult<T> LengthIsEqual<T>(this ValidationResult<T> result, int value) where T : IEnumerable
            => result.Validate(
                x => (x is null) || x.Count() == value,
                x => new ValidationError(ValidationErrorCode.LengthNotEqual.ToString("G"), x, new Dictionary<string, object>() { ["value"] = value })
            );

        public static ValidationResult<T> LengthIsGreater<T>(this ValidationResult<T> result, int min) where T : IEnumerable
            => result.Validate(
                x => (x is null) || x.Count() > min,
                x => new ValidationError(ValidationErrorCode.LengthNotGreater.ToString("G"), x, new Dictionary<string, object>() { ["min"] = min })
            );

        public static ValidationResult<T> LengthIsLess<T>(this ValidationResult<T> result, int max) where T : IEnumerable
            => result.Validate(
                x => (x is null) || x.Count() < max,
                x => new ValidationError(ValidationErrorCode.LengthNotLess.ToString("G"), x, new Dictionary<string, object>() { ["max"] = max })
            );

        public static ValidationResult<T> IsGreater<T>(this ValidationResult<T> result, T min) where T : IComparable<T>
            => result.Validate(
                x => (x is null) || x.CompareTo(min) > 0,
                x => new ValidationError(ValidationErrorCode.NotGreater.ToString("G"), x, new Dictionary<string, object>() { ["min"] = min })
                );
        public static ValidationResult<T> IsLess<T>(this ValidationResult<T> result, T max) where T : IComparable<T>
            => result.Validate(
                x => (x is null) || x.CompareTo(max) < 0,
                x => new ValidationError(ValidationErrorCode.NotLess.ToString("G"), x, new Dictionary<string, object>() { ["max"] = max })
            );

        public static ValidationResult<T> IsGreaterOrEquals<T>(this ValidationResult<T> result, T min) where T : IComparable<T>
            => result.Validate(
                x => (x is null) || x.CompareTo(min) >= 0,
                x => new ValidationError(ValidationErrorCode.NotGreaterOrEqual.ToString("G"), x, new Dictionary<string, object>() { ["min"] = min })
            );

        public static ValidationResult<T> IsLessOrEquals<T>(this ValidationResult<T> result, T max) where T : IComparable<T>
            => result.Validate(
                x => (x is null) || x.CompareTo(max) <= 0,
                x => new ValidationError(ValidationErrorCode.NotLessOrEqual.ToString("G"), x, new Dictionary<string, object>() { ["max"] = max })
            );

        public static ValidationResult<T> IsEquals<T>(this ValidationResult<T> result, T value) where T : IEquatable<T>
            => result.Validate(
                x => (x is null) || x.Equals(value),
                x => new ValidationError(ValidationErrorCode.NotEqual.ToString("G"), x, new Dictionary<string, object>() { ["rhs"] = value })
            );

        public static ValidationResult<T> NotEquals<T>(this ValidationResult<T> result, T value) where T : IEquatable<T>
            => result.Validate(
                x => (x is null) || !x.Equals(value),
                x => new ValidationError(ValidationErrorCode.IsEqual.ToString("G"), x, new Dictionary<string, object>() { ["rhs"] = value })
            );

        public static ValidationResult<string> IsMatch(this ValidationResult<string> result, string pattern, RegexOptions options = RegexOptions.None)
            => result.Validate(
                x => (x is null) || Regex.IsMatch(x, pattern, options),
                x => new ValidationError(ValidationErrorCode.NotMatch.ToString("G"), x, new Dictionary<string, object>() { ["pattern"] = pattern })
            );

        public static ValidationResult<T> NotEmpty<T>(this ValidationResult<T> result) where T : IEnumerable
            => result.LengthIsGreater(0);

        private static int Count(this IEnumerable enumerable)
        {
            int i = 0;
            foreach (var _ in enumerable)
            {
                i++;
            }
            return i;
        }
    }
}